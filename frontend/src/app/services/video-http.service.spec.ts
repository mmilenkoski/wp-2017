import { TestBed, inject } from '@angular/core/testing';

import { VideoHttpService } from './video-http.service';

describe('VideoHttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VideoHttpService]
    });
  });

  it('should be created', inject([VideoHttpService], (service: VideoHttpService) => {
    expect(service).toBeTruthy();
  }));
});
