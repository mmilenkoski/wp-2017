///<reference path="VideoServiceInterface.ts"/>
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {VideoServiceInterface} from './VideoServiceInterface';

import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Video} from '../../model/Video';


import {Observable} from 'rxjs/Observable';

import {tap} from 'rxjs/operators';
import {Category} from "../../model/Category";


@Injectable()
export class VideoHttpService extends VideoServiceInterface {


  private baseUrl = 'http://localhost:8080/api/';
  private loadUrl = this.baseUrl + 'videos';
  private videoSource = new BehaviorSubject<Video[]>([]);
  private observedVideos = this.videoSource.asObservable();

  private categorySource = new BehaviorSubject<Category[]>([]);
  private observedCategories = this.categorySource.asObservable();


  private loadCategoriesUrl = this.baseUrl + "categories";

  constructor(private http: HttpClient) {
    super();
  }


  public load(): Observable<Video[]> {
    this.http.get<Video[]>(this.loadUrl)
      .subscribe(
        videos => {
          this.videoSource.next(videos);
        }
      );
    return this.observedVideos;
  }

  public save(video: Video): Observable<Video> {
    const data = new FormData();
    data.set("title", video.title);
    data.set("description", video.description);
    data.set("url", video.url);
    data.set("categoryId", video.category);

    return this.http.post<Video>(this.loadUrl, data)
      .pipe(
        tap(video => this.load())
      );

  }


  edit(originalVideo: Video, updatedVideo: Video): Observable<Video> {
    const data = new FormData();
    data.set("newTitle", updatedVideo.title);
    data.set("newDescription", updatedVideo.description);

    return this.http.patch<Video>(this.loadUrl+`/${updatedVideo.id}`, data)
      .pipe(
        tap(video => this.load())
      );
  }

  findByTitle(title: string): Observable<Video[]> {
    return this.http.get<Video[]>(this.loadUrl + `?title=${title}`);
  }

  addTag(videoId: number, tagName: string): Observable<any> {
    return undefined;
  }

  removeTag(videoId: number, tagName: string): Observable<any> {
    return undefined;
  }

  loadCategories(): Observable<Category[]> {
    this.http.get<Category[]>(this.loadCategoriesUrl)
      .subscribe(
        categories => {
          this.categorySource.next(categories);
        }
      );
    return this.observedCategories;
  }
}
