package mk.ukim.finki.wp.videoaggregator.model;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.Id;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;

/**
 * @author Riste Stojanov
 */

@Entity
@Table(name = "users")
@Getter @Setter
public class User {

  @Id
  @GeneratedValue
  @Column(name = "id")
  public Long id;

  public String username;

  public String password;

  public String email;

  public Role role;

  public boolean active = false;

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("User{");
    sb.append("username='").append(username).append('\'');
    sb.append(", email='").append(email).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
