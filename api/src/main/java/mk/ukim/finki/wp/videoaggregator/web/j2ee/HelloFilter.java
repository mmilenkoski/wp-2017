package mk.ukim.finki.wp.videoaggregator.web.j2ee;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author Riste Stojanov
 */
@WebFilter(value = "/hello")
public class HelloFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        httpRequest
                .getSession()
                .setAttribute("browser", httpRequest.getHeader("User-Agent"));

        httpRequest.setAttribute("ip", httpRequest.getRemoteAddr());

        System.out.println("Before chain.doFilter");
        chain.doFilter(request, response);
        System.out.println("After chain.doFilter");
    }

    @Override
    public void destroy() {

    }
}
