package mk.ukim.finki.wp.videoaggregator.service;

import mk.ukim.finki.wp.videoaggregator.model.Tag;

/**
 * @author Riste Stojanov
 */
public interface TagService {

  Tag save(Tag tag);
}
