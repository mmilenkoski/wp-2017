package mk.ukim.finki.wp.videoaggregator.service.impl;

import mk.ukim.finki.wp.videoaggregator.model.Category;
import mk.ukim.finki.wp.videoaggregator.persistence.CategoryRepository;
import mk.ukim.finki.wp.videoaggregator.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

  private CategoryRepository categoryRepository;

  @Autowired
  public CategoryServiceImpl(CategoryRepository categoryRepository){
    this.categoryRepository = categoryRepository;
  }

  @Override
  public Optional<Category> findOne(Long categoryId) {
    return categoryRepository.findOne(categoryId);
  }

  @Override
  public Category save(Category category) {
    return categoryRepository.save(category);
  }

  @Override
  public Iterable<Category> findAll() {
    return categoryRepository.findAll();
  }

  @Override
  public Iterable<Category> findByTitle(String title) {
    return categoryRepository.findByTitle(title);
  }

  @Override
  public Iterable<Category> findByTitleLike(String title) {
    return categoryRepository.findByTitleLike(title);
  }

  @Override
  public void delete(Long id) {
    categoryRepository.delete(id);
  }
}
