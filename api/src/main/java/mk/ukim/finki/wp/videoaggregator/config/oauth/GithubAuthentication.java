package mk.ukim.finki.wp.videoaggregator.config.oauth;

import mk.ukim.finki.wp.videoaggregator.config.SecurityConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.UrlAuthorizationConfigurer;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;

@Component
@Configuration
@OAuthProvider
public class GithubAuthentication extends AbstractHttpConfigurer<UrlAuthorizationConfigurer<HttpSecurity>, HttpSecurity> {

  private static final String GITHUB_LOGIN = "/login/github";
  private OAuth2ClientContext oauth2ClientContext;

  @Autowired
  public GithubAuthentication(@Qualifier("oauth2ClientContext") OAuth2ClientContext oauth2ClientContext) {
    this.oauth2ClientContext = oauth2ClientContext;
  }

  @Override
  public void configure(HttpSecurity http) {
    http.addFilterBefore(ssoFilter(), BasicAuthenticationFilter.class);
  }

  private Filter ssoFilter() {
    SecurityConfiguration.ClientResources github = github();
    return SecurityConfiguration.ssoFilter(oauth2ClientContext, github, GITHUB_LOGIN);
  }

  @Bean
  @ConfigurationProperties("github")
  public SecurityConfiguration.ClientResources github() {
    return new SecurityConfiguration.ClientResources();
  }
}
