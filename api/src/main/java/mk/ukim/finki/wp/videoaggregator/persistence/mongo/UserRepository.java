package mk.ukim.finki.wp.videoaggregator.persistence.mongo;

import mk.ukim.finki.wp.videoaggregator.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Optional;

/**
 * @author Riste Stojanov
 */
public interface UserRepository extends MongoRepository<User, String> {

  User findByUsername(String username);
}
