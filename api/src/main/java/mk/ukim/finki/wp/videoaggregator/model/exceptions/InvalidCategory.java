package mk.ukim.finki.wp.videoaggregator.model.exceptions;

/**
 * @author Riste Stojanov
 */
public class InvalidCategory extends RuntimeException {
    public InvalidCategory() {
        super("Some message");
    }
}
