package mk.ukim.finki.wp.videoaggregator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class VideoAggregatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(VideoAggregatorApplication.class, args);
    }
}
