package mk.ukim.finki.wp.videoaggregator.config.captcha;

import mk.ukim.finki.wp.videoaggregator.model.exceptions.CaptchaException;

public interface CaptchaService {

  void processResponse(final String response, final String clientIP) throws CaptchaException;

  String getReCaptchaSite();

  String getReCaptchaSecret();

}
