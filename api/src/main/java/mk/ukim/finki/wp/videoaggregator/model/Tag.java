package mk.ukim.finki.wp.videoaggregator.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.search.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * @author Riste Stojanov
 */
@Indexed
@Entity
@Table(name = "tags")
public class Tag {

  @Field(name = "tagName", index = Index.YES, store = Store.NO, analyze = Analyze.YES)
  @Analyzer(definition = "emtAnalyser")
  @Boost(1f)
  @Id
  public String name;

  @JsonIgnore
  @ManyToMany(mappedBy = "tags")
  public List<Video> videos;


  public Tag(String name) {
    this.name = name;
  }

  public Tag() {

  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Tag tag = (Tag) o;

    return name.equals(tag.name);
  }
}
