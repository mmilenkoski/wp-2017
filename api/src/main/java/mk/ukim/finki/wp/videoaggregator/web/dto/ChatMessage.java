package mk.ukim.finki.wp.videoaggregator.web.dto;

import java.io.Serializable;

/**
 * Created by ristes on 4/27/16.
 */
public class ChatMessage implements Serializable {
  public String user;

  public String message;

  public String time;
}
