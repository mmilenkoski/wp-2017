package mk.ukim.finki.wp.videoaggregator.registration;

import mk.ukim.finki.wp.videoaggregator.model.User;
import mk.ukim.finki.wp.videoaggregator.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class SendMailOnRegister {

  private final UserService userService;

  private final JavaMailSender mailSender;

  @Autowired
  public SendMailOnRegister(UserService userService, JavaMailSender mailSender) {
    this.userService = userService;
    this.mailSender = mailSender;
  }

  @EventListener
  public void onApplicationEvent(OnRegisterCompletedEvent event) {
    User user = event.getUser();
    String randomToken = UUID.randomUUID().toString();

    // Generate token
    userService.createVerificationToken(user, randomToken);

    // Send confirmation email
    String userEmail = user.getEmail();
    String subject = "Activate your account";
    String confirmationURL = event.getAppUrl() + "/registrationConfirm?token=" + randomToken;

    SimpleMailMessage email = new SimpleMailMessage();
    email.setFrom("jdapps2016@gmail.com");
    email.setTo(userEmail);
    email.setSubject(subject);
    email.setText("Activate your account using the following url" + " \n" + "http://localhost:8080" + confirmationURL);
    mailSender.send(email);
  }
}
