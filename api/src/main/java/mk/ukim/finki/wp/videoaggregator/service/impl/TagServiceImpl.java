package mk.ukim.finki.wp.videoaggregator.service.impl;

import mk.ukim.finki.wp.videoaggregator.model.Tag;
import mk.ukim.finki.wp.videoaggregator.persistence.TagRepository;
import mk.ukim.finki.wp.videoaggregator.persistence.VideoRepository;
import mk.ukim.finki.wp.videoaggregator.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Riste Stojanov
 */
@Service
public class TagServiceImpl implements TagService {

  private final TagRepository tagRepository;

  private final VideoRepository videoRepository;

  @Autowired
  public TagServiceImpl(TagRepository tagRepository, VideoRepository videoRepository) {
    this.tagRepository = tagRepository;
    this.videoRepository = videoRepository;
  }

  @Override
  public Tag save(Tag tag) {
//    tag.videos
//      .forEach(
//        v -> videoRepository.save(v)
//      );


    return tagRepository.save(tag);
  }
}
