package mk.ukim.finki.wp.videoaggregator.init;

import mk.ukim.finki.wp.videoaggregator.model.Role;
import mk.ukim.finki.wp.videoaggregator.model.User;
import mk.ukim.finki.wp.videoaggregator.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author Riste Stojanov
 */
@Component
public class UserInit {
  private static Logger logger = LoggerFactory.getLogger(UserInit.class);

  private final UserService userService;

  private final PasswordEncoder passwordEncoder;

  private final Environment env;

  @Value("${app.user.admin.password}")
  private String adminPassword;


  public UserInit(UserService userService, PasswordEncoder passwordEncoder, Environment env) {
    this.userService = userService;
    this.passwordEncoder = passwordEncoder;
    this.env = env;
  }

  @PostConstruct
  public void init() {
    logger.debug("Initializing user");
    if (userService.numberOfUsers() == 0) {
      User user = new User();
      user.email = env.getProperty("app.user.admin.email");
      user.username = env.getProperty("app.user.admin.username");
      user.password = passwordEncoder.encode(adminPassword);
      user.active = true;
      user.role = Role.ROLE_WEB_USER;
      userService.save(user);

    }

  }
}
