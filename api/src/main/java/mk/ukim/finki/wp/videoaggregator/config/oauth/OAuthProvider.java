package mk.ukim.finki.wp.videoaggregator.config.oauth;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OAuthProvider {
}
