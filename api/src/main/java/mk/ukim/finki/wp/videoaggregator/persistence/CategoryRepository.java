package mk.ukim.finki.wp.videoaggregator.persistence;

import mk.ukim.finki.wp.videoaggregator.model.Category;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * @author Riste Stojanov
 */
public interface CategoryRepository {

  Optional<Category> findOne(Long categoryId);

  Category save(Category category);

  @Cacheable
  Iterable<Category> findAll();

  Iterable<Category> findByTitle(String title);

  Iterable<Category> findByTitleLike(String title);

  void delete(long id);

  void softDelete(long id);

  long count();
}
