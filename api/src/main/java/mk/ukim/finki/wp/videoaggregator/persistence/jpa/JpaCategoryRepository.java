package mk.ukim.finki.wp.videoaggregator.persistence.jpa;

import mk.ukim.finki.wp.videoaggregator.model.Category;
import mk.ukim.finki.wp.videoaggregator.persistence.CategoryRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

/**
 * @author Riste Stojanov
 */
@Profile("jpa")
public interface JpaCategoryRepository extends CategoryRepository,
  Repository<Category, Long> {


  @Modifying
  @Query("delete from #{#entityName} where id=:id")
  void delete(@Param("id") long id);


  @Modifying
  @Query("update #{#entityName} " +
    "set deleted=true " +
    "where id=:id")
  void softDelete(@Param("id") long id);

}
