package mk.ukim.finki.wp.videoaggregator.web.controllers;

import mk.ukim.finki.wp.videoaggregator.config.captcha.CaptchaService;
import mk.ukim.finki.wp.videoaggregator.config.thymeleaf.Layout;
import mk.ukim.finki.wp.videoaggregator.model.User;
import mk.ukim.finki.wp.videoaggregator.model.UserDto;
import mk.ukim.finki.wp.videoaggregator.model.UserVerificationToken;
import mk.ukim.finki.wp.videoaggregator.model.exceptions.CaptchaException;
import mk.ukim.finki.wp.videoaggregator.model.exceptions.UserNotFoundException;
import mk.ukim.finki.wp.videoaggregator.registration.OnRegisterCompletedEvent;
import mk.ukim.finki.wp.videoaggregator.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static mk.ukim.finki.wp.videoaggregator.service.impl.UserServiceImpl.VALID_TOKEN;

@Controller
public class UserController {

  private final CaptchaService captchaService;

  private final UserService userService;

  private final ApplicationEventPublisher eventPublisher;

  private final Environment environment;

  private final JavaMailSender mailSender;

  @Autowired
  public UserController(CaptchaService captchaService, UserService userService, ApplicationEventPublisher eventPublisher, Environment environment, JavaMailSender mailSender) {
    this.captchaService = captchaService;
    this.userService = userService;
    this.eventPublisher = eventPublisher;
    this.environment = environment;
    this.mailSender = mailSender;
  }

  @Layout("layouts/master")
  @GetMapping("/login")
  public String login() {
    return "fragments/login";
  }

  @Layout("layouts/master")
  @RequestMapping(value = "/register", method = RequestMethod.GET)
  public String showRegistrationForm(Model model) {
    UserDto userDto = new UserDto();
    model.addAttribute("user", userDto);
    return "fragments/register";
  }

  @Layout("layouts/master")
  @RequestMapping(value = "/register", method = RequestMethod.POST)
  public ModelAndView registerUserAccount(@ModelAttribute("user") @Valid final UserDto userDto, BindingResult bindingResult, Model model, HttpServletRequest httpServletRequest) {
    if (bindingResult.hasErrors()) {
      model.addAttribute("validationErrors", bindingResult.getAllErrors());
      if (!userDto.getPassword().equals(userDto.getMatchingPassword())) {
        model.addAttribute("passNoMatch", "Passwords don't match!");
      }
      model.addAttribute("user", userDto);
      return new ModelAndView("fragments/register", "user", userDto);
    } else {

      try {
        String response = httpServletRequest.getParameter("g-recaptcha-response");
        captchaService.processResponse(response, getClientIP(httpServletRequest));
      } catch (CaptchaException e) {
        return new ModelAndView("fragments/register", "user", userDto);
      }

      User registered = userService.registerNewUser(userDto);

      if (registered == null) {
        bindingResult.rejectValue("email", "message.regError");
      }

      if (bindingResult.hasErrors()) {
        return new ModelAndView("fragments/register", "user", userDto);
      } else {

        try {
          String appUrl = httpServletRequest.getProtocol() + "://" + httpServletRequest.getServerName() + ":" + httpServletRequest.getServerPort();
          eventPublisher.publishEvent(new OnRegisterCompletedEvent
            (registered, httpServletRequest.getLocale(), appUrl));
        } catch (Exception me) {
          bindingResult.rejectValue("email", "message.regError");
          return new ModelAndView("fragments/register", "user", userDto);
        }

        model.addAttribute("user", registered);
        return new ModelAndView("fragments/confirmation-email", "user", registered);
      }

    }
  }

  @RequestMapping(value = "/registrationConfirm", method = RequestMethod.GET)
  public String confirmRegistration(Model model, @RequestParam("token") String token) {

    UserVerificationToken verificationToken = userService.getToken(token);
    if (verificationToken == null) {
      String message = "Invalid token";
      model.addAttribute("message", message);
      return "redirect:/badUser";
    }

    if (userService.validateVerificationToken(token).equals(VALID_TOKEN)) {
      return "redirect:/login";
    } else {
      String messageValue = "Invalid";
      model.addAttribute("message", messageValue);
      return "redirect:/badUser";
    }
  }

  @Layout("layouts/master")
  @RequestMapping(value = "/reset-password", method = RequestMethod.GET)
  public String resetPassword(@RequestParam(required = false) String email, Model model) {
    if (email != null) {

      Optional<User> user = userService.findByEmail(email);
      if (!user.isPresent()) {
        throw new UserNotFoundException();
      }

      String token = UUID.randomUUID().toString();
      userService.createPasswordResetTokenForUser(user.get(), token);

      // Send reset password email
      mailSender.send(constructResetTokenEmail("http://localhost:8080", token, user.get()));

      model.addAttribute("confirmationEmail", email);
    }
    return "fragments/reset-password";
  }

  @RequestMapping(value = "/change-password", method = RequestMethod.GET)
  public String showChangePasswordPage(final Model model, @RequestParam("id") final Long id, @RequestParam("token") final String token) {
    final String result = userService.validatePasswordResetToken(id, token);
    if (result != null) {
      return "redirect:/login";
    }
    return "redirect:/edit-profile-change-password";
  }

  @Layout("layouts/master")
  @GetMapping("/profile")
  public String profile(Model model, Principal principal) {

    String userName = principal.getName();
    User u = userService.findByUsername(userName);

    model.addAttribute("user", u);
    return "fragments/user-profile";
  }

  @Layout("layouts/master")
  @RequestMapping(value = "/edit-profile-change-password", method = RequestMethod.GET)
  public String changeProfilePassword() {
    return "fragments/edit-profile-change-password";
  }

  @Layout("layouts/master")
  @RequestMapping(value = "/edit-profile-change-password", method = RequestMethod.POST)
  public String proceedChangeProfilePassword(@RequestParam Map<String, String> passwordDto) {
    User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    userService.changeUserPassword(user, passwordDto.get("password"));
    return "redirect:/login";
  }

  private String getClientIP(HttpServletRequest request) {
    final String xfHeader = request.getHeader("X-Forwarded-For");
    if (xfHeader == null) {
      return request.getRemoteAddr();
    }
    return xfHeader.split(",")[0];
  }

  private SimpleMailMessage constructResetTokenEmail(String contextPath, String token, User user) {
    String url = contextPath + "/change-password?id=" +
      user.getId() + "&token=" + token;
    String message = "Reset your password";
    return constructEmail(message + " \r\n" + url, user);
  }

  private SimpleMailMessage constructEmail(String body, User user) {
    SimpleMailMessage email = new SimpleMailMessage();
    email.setSubject("Reset Password");
    email.setText(body);
    email.setTo(user.getEmail());
    email.setFrom(environment.getProperty("from.email"));
    return email;
  }

}
