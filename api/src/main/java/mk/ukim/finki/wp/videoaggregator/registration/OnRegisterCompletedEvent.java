package mk.ukim.finki.wp.videoaggregator.registration;

import lombok.Getter;
import lombok.Setter;
import mk.ukim.finki.wp.videoaggregator.model.User;
import org.springframework.context.ApplicationEvent;

import java.util.Locale;

@Getter @Setter
public class OnRegisterCompletedEvent extends ApplicationEvent {
  private String appUrl;
  private Locale locale;
  private User user;

  public OnRegisterCompletedEvent(
    User user, Locale locale, String appUrl) {
    super(user);

    this.user = user;
    this.locale = locale;
    this.appUrl = appUrl;
  }

}
