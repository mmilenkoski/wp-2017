package mk.ukim.finki.wp.videoaggregator.service.impl;

import mk.ukim.finki.wp.videoaggregator.model.Category;
import mk.ukim.finki.wp.videoaggregator.persistence.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Riste Stojanov
 */
@Repository
public class CacheTestHelper implements ICacheTestHelper {

  @Autowired
  CategoryRepository repository;

  @Override
  @Transactional
  public Category save(Category category) {
    return repository.save(category);
  }

  @Override
  @Transactional(readOnly = true)
  public Category findOne(Long id) {
    return repository.findOne(id).get();
  }

  @Override
  @Transactional
  public void delete(Long id) {
    repository.delete(id);
  }
}
