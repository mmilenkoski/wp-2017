package mk.ukim.finki.wp.videoaggregator.service.impl;

import mk.ukim.finki.wp.videoaggregator.model.Category;
import org.springframework.stereotype.Service;

/**
 * @author Riste Stojanov
 */
public interface ICacheTestHelper {

  Category save(Category category);

  Category findOne(Long id);

  void delete(Long id);
}
