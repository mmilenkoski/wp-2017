package mk.ukim.finki.wp.videoaggregator;

import com.jayway.restassured.http.ContentType;
import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.persistence.VideoRepository;
import org.apache.http.HttpStatus;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

/**
 * @author Riste Stojanov
 */
@ActiveProfiles({"test", "jpa"})
public class VideoControllerTest extends CategoryResourceTest {

  @Autowired
  VideoRepository videoRepository;

  private Video video;

  @BeforeClass
  public void setup() {
    video = new Video();
    video.title = "test";
    video.url = "http://youtube.com/q=test";
    video.description = "test";
    video.category = category;
    video = videoRepository.save(video);

  }


  @Test
  public void test_create_video_success() {
    given().accept(ContentType.JSON)
      .when()
      .log().all()
      .param("title", "test video")
      .param("url", "http://youtube.com/q=1234")
      .param("description", "test video description")
      .param("categoryId", "1")
      .post("/api/videos")
      .then()
      .log().all()
      .assertThat()
      .statusCode(HttpStatus.SC_OK)
      .contentType(ContentType.JSON)
      .body(is(not(isEmptyOrNullString())));
  }

  @Test
  public void test_create_video_invalid_url() {
    given().accept(ContentType.JSON)
      .when()
      .log().all()
      .param("title", "test video")
      .param("url", "http://yot.com/q=1234")
      .param("description", "test video description")
      .param("categoryId", "1")
      .post("/api/videos")
      .then()
      .log().all()
      .assertThat()
      .statusCode(HttpStatus.SC_BAD_REQUEST)
      .contentType(ContentType.JSON)
      .body(is(not(isEmptyOrNullString())));
  }


}
